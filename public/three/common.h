
#ifndef THREE_COMMON_H_
#define THREE_COMMON_H_

#include <base/common.h>
#include <graphics/color.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <memory>

using Matrix = glm::mat4;
using Float = float;
using Time = float;

#endif // THREE_COMMON_H_
